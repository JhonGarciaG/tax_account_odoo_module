# -*- coding: utf-8 -*-
{
    'name': "Tax Account module",
    'version': '1.0 - dev',
    'author': "jgarcia@lsv-tech.com",
    'website': 'https://www.lsv-tech.com',
    'category': 'Accounting/Accounting',
    'summary': "Module to set tax in an Account",
    'depends':['account'],
    'data':['views/res_partner_view.xml'],
    'installable': True,
    'auto_install':False,
    'application': False,
    'license': 'LGPL-3'
}




